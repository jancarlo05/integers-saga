package com.miclat.mathsaga;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.miclat.mathsaga.Objects.MyMediaPlayer;

public class AboutUsActivity extends AppCompatActivity {


    private MyMediaPlayer myMediaPlayer;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myMediaPlayer = MyMediaPlayer.getInstance();
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (myMediaPlayer!=null){
            myMediaPlayer.pauseMusic();
        }
        System.out.println("TUTORIAL ON PAUSE");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (myMediaPlayer!=null){
            myMediaPlayer.resumeMusic();
        }
        System.out.println("TUTORIAL ON RESUME");
    }
}