package com.miclat.mathsaga.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.miclat.mathsaga.Objects.MyListener;
import com.miclat.mathsaga.R;

import java.util.ArrayList;

public class DifficultyRecyclerViewAdapter extends RecyclerView.Adapter<DifficultyRecyclerViewAdapter.ViewHolder>{

    private ArrayList<String> arrayList;
    private final Context context;
    private MyListener.DifficultyListener listener;

    // RecyclerView recyclerView;
    public DifficultyRecyclerViewAdapter(ArrayList<String> arrayList, Context context, MyListener.DifficultyListener listener) {
        this.arrayList = arrayList;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.difficulty_recyclerview_cell, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String item = arrayList.get(position);

        holder.title.setText(item);

        if (item.contains("Easy")){
            holder.title.setBackgroundResource(R.drawable.gradient_green_2);
        }
        else if (item.contains("Average")){
            holder.title.setBackgroundResource(R.drawable.gradient_blue);
        }
        else if (item.contains("Hard")){
            holder.title.setBackgroundResource(R.drawable.gradient_red);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDifficultyListener(item);
            }
        });

        setAnimation(holder.itemView);

    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);

        }
    }

    private void setAnimation(View viewToAnimate)
    {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.right_to_left);
        viewToAnimate.startAnimation(animation);
    }
}