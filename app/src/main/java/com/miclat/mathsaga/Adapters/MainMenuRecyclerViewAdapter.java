package com.miclat.mathsaga.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.miclat.mathsaga.Objects.MyListener;
import com.miclat.mathsaga.R;

import java.util.ArrayList;

public class MainMenuRecyclerViewAdapter extends RecyclerView.Adapter<MainMenuRecyclerViewAdapter.ViewHolder>{

    private final ArrayList<String> arrayList;
    private final Context context;
    private final MyListener.MenuListener menuListener;

    // RecyclerView recyclerView;
    public MainMenuRecyclerViewAdapter(ArrayList<String> arrayList, Context context,MyListener.MenuListener menuListener) {
        this.arrayList = arrayList;
        this.context = context;
        this.menuListener = menuListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.menu_recyclerview_cell, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String item = arrayList.get(position);
        holder.title.setText(item);

        if (item.equalsIgnoreCase("Let's Practice")){
            Glide.with(context).load(R.drawable.practice).into(holder.icon);
        }
        else if (item.equalsIgnoreCase("Let's Play")){
            Glide.with(context).load(R.drawable.calculations).into(holder.icon);
        }
        else if (item.equalsIgnoreCase("High Scores")){
            Glide.with(context).load(R.drawable.score).into(holder.icon);
        }
        else if (item.equalsIgnoreCase("About Us")){
            Glide.with(context).load(R.drawable.info).into(holder.icon);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuListener.onMenuListenerCallback(item);
            }
        });

    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            title = itemView.findViewById(R.id.title);

        }
    }
}