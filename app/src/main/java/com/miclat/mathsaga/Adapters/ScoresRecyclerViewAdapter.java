package com.miclat.mathsaga.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.miclat.mathsaga.Objects.MyListener;
import com.miclat.mathsaga.Objects.Scores;
import com.miclat.mathsaga.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ScoresRecyclerViewAdapter extends RecyclerView.Adapter<ScoresRecyclerViewAdapter.ViewHolder>{

    private final ArrayList<Scores> arrayList;
    private final Context context;


    // RecyclerView recyclerView;

    public ScoresRecyclerViewAdapter(ArrayList<Scores> arrayList, Context context, MyListener.SubjectListener listener) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.score_recycler_adapter, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Scores item = arrayList.get(position);
        holder.name.setText(item.name);
        holder.score.setText(item.score+"");
        holder.operation.setText(item.operations);
        holder.difficulty.setText(item.difficulty);

        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy hh:mm a");
        if (item.date!=null){
            holder.date.setText(format.format(item.date));
        }else {
            holder.date.setText("--");
        }

        setAnimation(holder.itemView);

    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView score,name,operation,date,difficulty;

        public ViewHolder(View itemView) {
            super(itemView);
            score = itemView.findViewById(R.id.score);
            name = itemView.findViewById(R.id.name);
            operation = itemView.findViewById(R.id.operation);
            date = itemView.findViewById(R.id.date);
            difficulty = itemView.findViewById(R.id.difficulty);
        }
    }

    private void setAnimation(View viewToAnimate)
    {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.right_to_left);
        viewToAnimate.startAnimation(animation);
    }
}