package com.miclat.mathsaga.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.miclat.mathsaga.Objects.MyListener;
import com.miclat.mathsaga.R;

import java.util.ArrayList;

public class SubjectRecyclerViewAdapter extends RecyclerView.Adapter<SubjectRecyclerViewAdapter.ViewHolder>{

    private final ArrayList<String> arrayList;
    private final Context context;
    private final MyListener.SubjectListener listener;


    // RecyclerView recyclerView;
    public SubjectRecyclerViewAdapter(ArrayList<String> arrayList, Context context, MyListener.SubjectListener listener) {
        this.arrayList = arrayList;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.subject_recyclerview_cell, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String item = arrayList.get(position);

        holder.title.setText(item);

        if (item.contains("Addition")){
            Glide.with(context).load(R.drawable.addition).into(holder.icon);
        }
        else if (item.contains("Subtraction")){
            Glide.with(context).load(R.drawable.subtraction).into(holder.icon);
        }
        else if (item.contains("Multiplication")){
            Glide.with(context).load(R.drawable.multiplication).into(holder.icon);
        }
        else if (item.contains("Division")){
            Glide.with(context).load(R.drawable.division).into(holder.icon);
        }
        else if (item.contains("Random")){
            Glide.with(context).load(R.drawable.question).into(holder.icon);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSubjectListener(item);
            }
        });

        setAnimation(holder.itemView);

    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            title = itemView.findViewById(R.id.title);

        }
    }

    private void setAnimation(View viewToAnimate)
    {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.right_to_left);
        viewToAnimate.startAnimation(animation);
    }
}