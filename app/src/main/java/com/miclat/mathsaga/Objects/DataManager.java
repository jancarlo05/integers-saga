package com.miclat.mathsaga.Objects;

import android.view.View;

import java.util.ArrayList;
import java.util.Comparator;

public class DataManager {
    private static DataManager instance;
    public String selectedDifficulty;
    public String selectedSubject;
    private ArrayList<Scores>scores;
    private String name;


    public String selectedTutorialOperation;
    public String selectedTutorial;


    public static DataManager getInstance() {
        if (instance == null){
            instance = new DataManager();
        }
        return instance;
    }

    public String getName() {
        if (name == null){
            name = "--";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Scores> getScores() {
        if (scores == null){
            scores = new ArrayList<>();
        }
        scores.sort(new Comparator<Scores>() {
            @Override
            public int compare(Scores o1, Scores o2) {
                Integer score1 = o1.score;
                Integer score2 = o2.score;
                return score2.compareTo(score1);
            }
        });
        return scores;
    }

    public void addScore(Scores score){
        if (scores == null){
            scores = new ArrayList<>();
        }
        scores.add(score);
        Pref.getInstance().setSharedpref();
    }

    public void setScores(ArrayList<Scores> scores) {
        this.scores = scores;
    }
}
