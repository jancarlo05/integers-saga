/*
 *
 */

package com.miclat.mathsaga.Objects;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by EmmanKusumi on 7/4/17.
 */

public class JSONDate extends Date
{
    static public Date convertJSONDateToNSDate(String jsonDate)
    {
        if(jsonDate == null)
        {
            return null;
        }
        else
        {
            int startPosition = jsonDate.lastIndexOf('(');
            int lastPosition = jsonDate.lastIndexOf(')');

            if(startPosition == -1 || lastPosition == -1)
            {
                return null;
            }
            else
            {
                long timeInterval = Long.parseLong(jsonDate.substring(startPosition + 1, lastPosition));

                if(timeInterval < 0)
                {
                    return null;
                }
                else
                {
                    Date date = new Date(timeInterval);
                    return date;
                }
            }
        }
    }



    public static String convertJavaDatetoJsonDate(Date date){
        String jsonStringdate = "";
        if (date!=null){
            jsonStringdate = DateTimeFormat.convertToJsonDateTime(date);
        }
        return jsonStringdate;
    }

    public static String convertJavaDateToISODate(Date date){
        String jsonStringdate = "";
        if (date!=null){
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            jsonStringdate = format.format(date);
        }
        return jsonStringdate;
    }

}
