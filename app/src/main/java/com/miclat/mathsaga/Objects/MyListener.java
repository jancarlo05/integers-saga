package com.miclat.mathsaga.Objects;

public class MyListener {

    public interface MenuListener{
        void onMenuListenerCallback(String Callback);
    }

    public interface SubjectListener{
        void onSubjectListener(String Callback);
    }

    public interface DifficultyListener{
        void onDifficultyListener(String Callback);
    }

    public interface CompleteListener{
        void onCompleteListener(boolean isNext);
    }

    public interface GameOverListener{
        void onCompleteListener(boolean isPlayAgain);
    }

}
