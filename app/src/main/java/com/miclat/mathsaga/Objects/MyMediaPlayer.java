package com.miclat.mathsaga.Objects;

import android.content.Context;
import android.media.MediaPlayer;

import com.miclat.mathsaga.R;

public class MyMediaPlayer {
    private static MyMediaPlayer instance;
    private Context context;
    private MediaPlayer player;;
    private int length;

    public void initMediaPlayer(Context context){
        getInstance().context = context;
        setMusic(R.raw.bgmusic);
        getInstance().player.setLooping(true);
        setVolume(1);
    }


    public void setVolume(float volume){
        if (getInstance().player!=null){
            getInstance().player.setVolume(volume,volume);
        }
    }

    public void setMusic(int music){
        if (getInstance().player!=null){
            getInstance().player.stop();
            getInstance().player.release();
            getInstance().player = MediaPlayer.create(context,music);
        }else {
            getInstance().player = MediaPlayer.create(context,music);
        }
    }

    public void startMusic() {
        if (getInstance().player!=null){
            if(getInstance().player.isPlaying())
            {
                getInstance().player.stop();
            }
            getInstance().player.start();
        }else {
            initMediaPlayer(getInstance().context);
            startMusic();
        }
    }

    public void pauseMusic() {
        if(getInstance().player.isPlaying())
        {
            getInstance().player.pause();
            getInstance().length=getInstance().player.getCurrentPosition();

        }
    }

    public void resumeMusic() {
        if(!getInstance().player.isPlaying())
        {
            getInstance().player.seekTo(getInstance().length);
            getInstance().player.start();
        }
    }



    public static MyMediaPlayer getInstance() {
        if (instance == null){
            instance = new MyMediaPlayer();
        }
        return instance;
    }

    public static void setInstance(MyMediaPlayer instance) {
        MyMediaPlayer.instance = instance;
    }
}
