package com.miclat.mathsaga.Objects;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

public class Pref {

    public static Pref Instance;
    private Context context;
    //
    private SharedPreferences preferences;

    public static Pref getInstance(){
        if (Instance == null) Instance = new Pref();
        return Instance;
    }

    public void Initialize(Context ctxt){
        context = ctxt;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setSharedpref() {
        try {
            SharedPreferences.Editor prefsEditor = preferences.edit();
            Gson gson = new Gson();
            DataManager dataManager = DataManager.getInstance();
            DataManager obj = new DataManager();

            obj.setScores(dataManager.getScores());
            String DataManager = gson.toJson(obj);
            prefsEditor.putString("DataManager", DataManager);
            prefsEditor.apply();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getSharedPref() {
        Gson gson = new Gson();
        String dataManager = preferences.getString("DataManager", "");
        DataManager old =  gson.fromJson(dataManager, DataManager.class);
        if (old!=null){
            DataManager.getInstance().setScores(old.getScores());
        }
    }


}
