package com.miclat.mathsaga.Objects;

import android.content.Context;
import android.util.DisplayMetrics;

import java.util.ArrayList;

public class Tools {

    public static int getMinMax(int number,boolean isMinimum){
        if (isMinimum){
            return number*-1;
        }
        return number;
    }


    public static int getSingleDigitRandomNumber(){
        int min = -10;
        int max = 10;
        return (int) (Math.random() * (max - min + 1) + min);
    }

    public static int getDoubleDigitRandomNumber(){
        int min = -20;
        int max = 20;
        return (int) (Math.random() * (max - min + 1) + min);
    }

    public static int getThreeDigitRandomNumber(){
        int min = -30;
        int max = 30;
        return (int) (Math.random() * (max - min + 1) + min);
    }

    public static int getDivisibleNumber(int number,int min,int max){
        ArrayList<Integer>integers = new ArrayList<>();

        System.out.println("NUMBER : "+number);
        System.out.println("min : "+min);
        System.out.println("max : "+max);


        for (int i=min; i< max * 3; i++) {
            if (i%number==0){
                if (i != 0){
                    integers.add(i);
                }
            }
        }

        System.out.println("DIVISIBLE : "+integers);

        int randomIndex = (int) (Math.random() * (integers.size() + 1) + 0);

        if (randomIndex == integers.size()){
            randomIndex = randomIndex-1;
        }
        if (integers.size() == 0){
            return number;
        }

        System.out.println("SElECTED INDEX : "+integers.get(randomIndex));
        return integers.get(randomIndex);
    }

    public static int getRandomForDivision(int min, int max){
        int random = (int) (Math.random() * (max - min + 1) + min);
        if (random != 0){
            System.out.println("getRandomForDivision : "+random + " | min : "+min+" | max : "+max);
            return random;
        }
        return getRandomForDivision(min,max);
    }

    public static float convertDpToPixel(float dp, Context context){
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

}
