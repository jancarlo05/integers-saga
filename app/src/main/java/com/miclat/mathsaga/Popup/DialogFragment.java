package com.miclat.mathsaga.Popup;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;

import com.miclat.mathsaga.Objects.DataManager;
import com.miclat.mathsaga.Objects.MyListener;
import com.miclat.mathsaga.Objects.Scores;
import com.miclat.mathsaga.QuizActivity;
import com.miclat.mathsaga.R;

import java.util.Calendar;
import java.util.Objects;

public class DialogFragment extends AlertDialog {

    private String type;

    private MyListener.CompleteListener completeListener;

    private MyListener.GameOverListener gameOverListener;

    private ImageView next,cancel;
    private TextView name,score,operation,difficulty;
    private int currentScore;

    public DialogFragment(@NonNull Context context) {
        super(context);
    }

    public void createCompletePopup(MyListener.CompleteListener completeListener){
        this.completeListener = completeListener;
        type = "Complete";
    }
    public void createGameOverPopup(MyListener.GameOverListener gameOverListener,int score){
        this.gameOverListener = gameOverListener;
        type = "GameOver";
        this.currentScore = score;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {


        if (type.equalsIgnoreCase("Complete")){
            MediaPlayer quizPlayer = MediaPlayer.create(getContext(),R.raw.correct);
            quizPlayer.setVolume(1,1);
            quizPlayer.start();

            setContentView(R.layout.next_alert_dialog);
            initNextPopup();
        }else {
            setContentView(R.layout.result_popup);
            initGameOverPopup();
        }

        Objects.requireNonNull(getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }



    private void initNextPopup(){
        next = findViewById(R.id.next);
        cancel = findViewById(R.id.exit);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (completeListener!=null){
                    completeListener.onCompleteListener(true);
                }
                dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (completeListener!=null){
                    completeListener.onCompleteListener(false);
                }
                dismiss();
            }
        });
    }

    private void initGameOverPopup(){

        score = findViewById(R.id.score);
        name = findViewById(R.id.name);
        next = findViewById(R.id.next);
        cancel = findViewById(R.id.exit);
        operation = findViewById(R.id.operation);
        difficulty = findViewById(R.id.difficulty);

        name.setText("Hi "+ DataManager.getInstance().getName()+"!");
        score.setText("Your Score is "+currentScore +" / 10");
        operation.setText("Operation: "+ DataManager.getInstance().selectedSubject);
        difficulty.setText("Difficulty: "+ DataManager.getInstance().selectedDifficulty);

        Scores score = new Scores();
        score.name = DataManager.getInstance().getName();
        score.date  = Calendar.getInstance().getTime();
        score.difficulty = DataManager.getInstance().selectedDifficulty;
        score.operations = DataManager.getInstance().selectedSubject;
        score.score = currentScore;

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gameOverListener!=null){
                    gameOverListener.onCompleteListener(true);
                }
                DataManager.getInstance().addScore(score);
                dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gameOverListener!=null){
                    gameOverListener.onCompleteListener(false);
                }
                DataManager.getInstance().addScore(score);
                dismiss();
            }
        });
    }
}
