package com.miclat.mathsaga.Popup;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.miclat.mathsaga.Objects.DataManager;
import com.miclat.mathsaga.R;

import java.util.Objects;

public class ResultPopup extends AlertDialog {

    private EditText editText;
    private TextView submit;

    public ResultPopup(@NonNull Context context) {
        super(context);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.result_popup);

        editText = findViewById(R.id.editText);
        submit = findViewById(R.id.submit);

        Objects.requireNonNull(getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataManager.getInstance().setName(editText.getText().toString());
                dismiss();
            }
        });
    }

}
