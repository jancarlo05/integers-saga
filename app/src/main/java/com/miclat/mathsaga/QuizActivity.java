package com.miclat.mathsaga;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.miclat.mathsaga.Objects.DataManager;
import com.miclat.mathsaga.Objects.MyListener;
import com.miclat.mathsaga.Objects.MyMediaPlayer;
import com.miclat.mathsaga.Objects.Tools;
import com.miclat.mathsaga.Popup.DialogFragment;

import java.util.ArrayList;

public class QuizActivity extends AppCompatActivity {


    private TextView timer, question, indicator, score, task;
    private EditText answer;
    private int firstNum, secNum, selectedAnswer, currentScore, questionCounter;
    private ImageView back, difficulty, subject, setting;
    private CardView cardView;
    private Handler countDownHandler;
    private int miliseconds;
    private DialogFragment dialog;
    private String selectedSubject;
    private int timerCountdown;

    private MyMediaPlayer myMediaPlayer;
    private MediaPlayer quizPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        timer = findViewById(R.id.timer);
        cardView = findViewById(R.id.cardView);
        difficulty = findViewById(R.id.difficulty);
        question = findViewById(R.id.question);
        answer = findViewById(R.id.answer);
        indicator = findViewById(R.id.indicator);
        score = findViewById(R.id.points);
        task = findViewById(R.id.task);
        subject = findViewById(R.id.subject);
        back = findViewById(R.id.back);
        setting = findViewById(R.id.imageView);

        dialog = new DialogFragment(QuizActivity.this);

        currentScore = 0;
        questionCounter = 0;
        back = findViewById(R.id.back);

        initCountDownTimer();

        initOnclick();

        myMediaPlayer = MyMediaPlayer.getInstance();
        myMediaPlayer.setVolume(.02f);
    }


    private void initOnclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void resetCountDownHandler() {
        if (countDownHandler != null) {
            countDownHandler.removeCallbacks(runnable(1000));
            countDownHandler.removeMessages(0);
        }
    }

    private void createCountDownHandler(int seconds, int secondInterval) {
        miliseconds = seconds * 1000;
        startCountdownHandler(secondInterval * 1000);
    }

    private void startCountdownHandler(int interval) {
        countDownHandler = new Handler(Looper.getMainLooper());
        countDownHandler.postDelayed(runnable(interval), 1000);
    }

    private Runnable runnable(int interval) {
        return new Runnable() {
            @Override
            public void run() {
                miliseconds -= interval;
                int seconds = (int) (miliseconds / 1000);
                timer.setText(seconds + " ");
                if (seconds <= 10){
                    if (quizPlayer == null){
                        quizPlayer = MediaPlayer.create(QuizActivity.this,R.raw.time_ticking);
                        quizPlayer.setVolume(1,1);
                        quizPlayer.setLooping(true);
                        quizPlayer.start();
                    }
                    timer.setTextColor(ContextCompat.getColor(QuizActivity.this,R.color.red2));
                }else {
                    if (quizPlayer != null){
                        if (quizPlayer.isPlaying()){
                            quizPlayer.pause();
                            quizPlayer.stop();
                            quizPlayer = null;
                        }
                    }
                    timer.setTextColor(ContextCompat.getColor(QuizActivity.this,R.color.black));
                }

                if (seconds > 1) {
                    timer.append("secs");
                } else {
                    timer.append("sec");
                }
                if (seconds == 0) {
                    initCountDownTimer();
                } else {
                    resetCountDownHandler();
                    startCountdownHandler(interval);
                }
            }
        };
    }

    private void initCountDownTimer() {

        questionCounter++;
        answer.setText("");
        indicator.setVisibility(View.GONE);

        initScore();

        if (questionCounter > 10) {
            dialog = new DialogFragment(this);
            dialog.createGameOverPopup(getGameOverListener(), currentScore);
            dialog.setCancelable(false);
            if (!dialog.isShowing()) {
                dialog.show();
            }
            return;
        }

        if (DataManager.getInstance().selectedSubject.equalsIgnoreCase("Random")){
            selectedSubject = getRandomOperation();
        }else {
            selectedSubject = DataManager.getInstance().selectedSubject;
        }

        initDifficulty();

        initAnswer();

        initQuestion();

        initNumberOfQuestions();

        resetCountDownHandler();

        createCountDownHandler(timerCountdown, 1);
    }

    private void initDifficulty() {
        DataManager dataManager = DataManager.getInstance();

        if (dataManager.selectedDifficulty != null) {
            if (dataManager.selectedDifficulty.equalsIgnoreCase("Easy")) {
                timerCountdown = 31;
                if (selectedSubject.equalsIgnoreCase("Division")){
                    firstNum = Tools.getRandomForDivision(1,10);
                    secNum = Tools.getDivisibleNumber(firstNum, -10, 10);
                }else {
                    firstNum = Tools.getSingleDigitRandomNumber();
                    secNum =Tools.getSingleDigitRandomNumber();
                }
                Glide.with(this).load(R.drawable.easy).into(difficulty);
                cardView.setCardBackgroundColor(ContextCompat.getColor(this, R.color.math4));
                Glide.with(this).load(R.drawable.grass).into(setting);
            } else if (dataManager.selectedDifficulty.equalsIgnoreCase("Average")) {
                timerCountdown = 26;
                if (selectedSubject.equalsIgnoreCase("Division")){
                    firstNum = Tools.getRandomForDivision(1,20);
                    secNum = Tools.getDivisibleNumber(firstNum, -20, 20);
                }else {
                    firstNum = Tools.getDoubleDigitRandomNumber();
                    secNum =Tools.getDoubleDigitRandomNumber();
                }
                Glide.with(this).load(R.drawable.average).into(difficulty);
                cardView.setCardBackgroundColor(ContextCompat.getColor(this, R.color.blue1));
                Glide.with(this).load(R.drawable.beach).into(setting);
            } else {
                timerCountdown = 21;
                if (selectedSubject.equalsIgnoreCase("Division")){
                    secNum = Tools.getRandomForDivision(1,30);
                    firstNum = Tools.getDivisibleNumber(secNum, -30, 30);
                }else {
                    firstNum = Tools.getThreeDigitRandomNumber();
                    secNum =Tools.getThreeDigitRandomNumber();
                }

                Glide.with(this).load(R.drawable.hard).into(difficulty);
                cardView.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red1));
                Glide.with(this).load(R.drawable.carnival).into(setting);
            }
        }
    }

    private void initQuestion() {
        String operation = getSelectedOperation();
        if (selectedSubject.equalsIgnoreCase("Division")) {
            question.setText(secNum + " ");
            question.append(operation + " ");
            if (firstNum < 0) {
                question.append("(" + firstNum + ") = ?");
            } else {
                question.append(firstNum + " = ?");
            }
        } else {
            question.setText(firstNum + " ");
            question.append(operation + " ");
            if (secNum < 0) {
                question.append("(" + secNum + ") = ?");
            } else {
                question.append(secNum + " = ?");
            }
        }
    }

    private void initAnswer() {
        String operation = getSelectedOperation();
        if (operation.equalsIgnoreCase("+")) {
            selectedAnswer = firstNum + secNum;
            Glide.with(this).load(R.drawable.addition_banner).into(subject);
        } else if (operation.equalsIgnoreCase("-")) {
            selectedAnswer = firstNum - secNum;
            Glide.with(this).load(R.drawable.s_banner).into(subject);
        } else if (operation.equalsIgnoreCase("x")) {
            selectedAnswer = firstNum * secNum;
            Glide.with(this).load(R.drawable.m_banner).into(subject);
        } else {
            selectedAnswer = secNum / firstNum;
            Glide.with(this).load(R.drawable.d_banner).into(subject);
        }

        indicator.setVisibility(View.GONE);
        answer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                indicator.setVisibility(View.GONE);
                if (s.length() > 0) {
                    if (s.toString().equalsIgnoreCase(selectedAnswer + "")) {
                        if (questionCounter == 10) {
                            if (!dialog.isShowing()) {
                                currentScore++;
                                resetCountDownHandler();
                                dialog = new DialogFragment(QuizActivity.this);
                                dialog.createGameOverPopup(getGameOverListener(), currentScore);
                                dialog.setCancelable(false);
                                dialog.show();
                            }

                        } else {
                            if (!dialog.isShowing()) {
                                currentScore++;
                                resetCountDownHandler();
                                dialog = new DialogFragment(QuizActivity.this);
                                dialog.createCompletePopup(getCompleteListener());
                                dialog.setCancelable(false);
                                indicator.setVisibility(View.GONE);
                                dialog.show();
                            }
                        }
                    } else {
                        indicator.setVisibility(View.VISIBLE);
                        indicator.setText("Incorrect Answer");
                        indicator.setTextColor(ContextCompat.getColor(QuizActivity.this, R.color.red2));
                    }
                } else {
                    indicator.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private MyListener.CompleteListener getCompleteListener() {
        return new MyListener.CompleteListener() {
            @Override
            public void onCompleteListener(boolean isNext) {
                if (isNext) {
                    resetCountDownHandler();
                    initCountDownTimer();
                } else {
                    finish();
                }
            }
        };
    }

    private MyListener.GameOverListener getGameOverListener() {
        return new MyListener.GameOverListener() {
            @Override
            public void onCompleteListener(boolean isPlayAgain) {
                if (isPlayAgain) {
                    currentScore = 0;
                    questionCounter = 0;
                    initCountDownTimer();
                } else {
                    finish();
                }
            }
        };
    }


    private String getSelectedOperation() {
        if (selectedSubject != null) {
            if (selectedSubject.equalsIgnoreCase("Addition")) return "+";
            if (selectedSubject.equalsIgnoreCase("Subtraction")) return "-";
            if (selectedSubject.equalsIgnoreCase("Multiplication")) return "x";
            if (selectedSubject.equalsIgnoreCase("Division")) return "÷";
        }
        return "+";
    }

    private void initScore() {
        score.setText("Score: " + currentScore);
    }

    private void initNumberOfQuestions() {
        task.setText("Answer the following question : ");
        task.append(questionCounter + " /10");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        resetCountDownHandler();
    }

    private String getRandomOperation(){
        ArrayList<String>operations = new ArrayList<>();
        operations.add("Addition");
        operations.add("Subtraction");
        operations.add("Multiplication");
        operations.add("Division");

        int min = 0;
        int max = operations.size()-1;

        int randomIndex = (int) (Math.random() * (max - min + 1) + min);

        return operations.get(randomIndex);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (myMediaPlayer!=null){
            myMediaPlayer.pauseMusic();
            myMediaPlayer.setVolume(1);
        }
        System.out.println("QUIZ ON PAUSE");
        if (quizPlayer!=null) {
            quizPlayer.stop();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (myMediaPlayer!=null){
            myMediaPlayer.resumeMusic();
            myMediaPlayer.setVolume(0.02f);
        }
        if (quizPlayer!=null) {
            quizPlayer.pause();
        }
    }

}