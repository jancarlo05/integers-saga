package com.miclat.mathsaga;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.miclat.mathsaga.Objects.DataManager;
import com.miclat.mathsaga.Objects.MyMediaPlayer;

public class TutorialActivity extends AppCompatActivity {

    ImageView back;
    View addition,subtraction,multiplication,division,addition_container,subtraction_container,multiplication_container,division_container;
    private MyMediaPlayer myMediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        addition = findViewById(R.id.addition);
        subtraction = findViewById(R.id.subtraction);
        multiplication = findViewById(R.id.multiplication);
        division = findViewById(R.id.division);

        addition.setVisibility(View.VISIBLE);
        subtraction.setVisibility(View.VISIBLE);
        multiplication.setVisibility(View.VISIBLE);
        division.setVisibility(View.VISIBLE);

        addition_container = findViewById(R.id.addition_container);
        subtraction_container = findViewById(R.id.subtraction_container);
        multiplication_container = findViewById(R.id.multiplication_container);
        division_container = findViewById(R.id.division_container);


        myMediaPlayer = MyMediaPlayer.getInstance();


        back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        addition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataManager.getInstance().selectedTutorial = "sjnojCCCcRk";
                DataManager.getInstance().selectedTutorialOperation = "Addition";
                Intent intent = new Intent(TutorialActivity.this,YoutubePlayerActivity.class);
                startActivity(intent);
            }
        });

        subtraction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataManager.getInstance().selectedTutorial = "Y_n-xQ3Re2A";
                DataManager.getInstance().selectedTutorialOperation = "Subtraction";
                Intent intent = new Intent(TutorialActivity.this,YoutubePlayerActivity.class);
                startActivity(intent);
            }
        });

        multiplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataManager.getInstance().selectedTutorial = "NyWnK5hVsfE";
                DataManager.getInstance().selectedTutorialOperation = "Multiplication";
                Intent intent = new Intent(TutorialActivity.this,YoutubePlayerActivity.class);
                startActivity(intent);
            }
        });

        division.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataManager.getInstance().selectedTutorial = "9zKXOzxJPUY";
                DataManager.getInstance().selectedTutorialOperation = "Division";
                Intent intent = new Intent(TutorialActivity.this,YoutubePlayerActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (myMediaPlayer!=null){
            myMediaPlayer.pauseMusic();
        }
        System.out.println("TUTORIAL ON PAUSE");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (myMediaPlayer!=null){
            myMediaPlayer.resumeMusic();
        }
        System.out.println("TUTORIAL ON RESUME");
    }


}