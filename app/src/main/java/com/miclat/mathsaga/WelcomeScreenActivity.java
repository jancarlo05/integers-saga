package com.miclat.mathsaga;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.miclat.mathsaga.Adapters.DifficultyRecyclerViewAdapter;
import com.miclat.mathsaga.Adapters.MainMenuRecyclerViewAdapter;
import com.miclat.mathsaga.Adapters.ScoresRecyclerViewAdapter;
import com.miclat.mathsaga.Adapters.SubjectRecyclerViewAdapter;
import com.miclat.mathsaga.Objects.BackgroundSoundService;
import com.miclat.mathsaga.Objects.DataManager;
import com.miclat.mathsaga.Objects.MyListener;
import com.miclat.mathsaga.Objects.MyMediaPlayer;
import com.miclat.mathsaga.Objects.Pref;
import com.miclat.mathsaga.Objects.Tools;
import com.miclat.mathsaga.Popup.RegisterPopup;

import java.util.ArrayList;

public class WelcomeScreenActivity extends AppCompatActivity {


    private RecyclerView recyclerview;
    private ImageView back;
    private TextView title;
    private BackgroundSoundService backgroundSoundService;
    private MyMediaPlayer myMediaPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        recyclerview = findViewById(R.id.recyclerview);
        back = findViewById(R.id.back);
        title = findViewById(R.id.textView);

        Pref.getInstance().Initialize(this);
        Pref.getInstance().getSharedPref();

        RegisterPopup registerPopup = new RegisterPopup(this);
        registerPopup.setCancelable(false);
        registerPopup.show();

        myMediaPlayer = MyMediaPlayer.getInstance();
        myMediaPlayer.initMediaPlayer(this);
        myMediaPlayer.startMusic();



        for (int i = 0; i <10 ; i++) {
            Tools.getRandomForDivision(-10,10);
        }


        initMainMenu();

    }

    private void initMainMenu(){
        back.setVisibility(View.GONE);
        title.setText("Integer Saga");

        ArrayList<String>data = new ArrayList<>();
        data.add("Let's Play");
        data.add("Tutorial");
        data.add("High Scores");
        data.add("About Us");

        MainMenuRecyclerViewAdapter adapter = new MainMenuRecyclerViewAdapter(data,this,getMenuListener());
        recyclerview.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        recyclerview.setAdapter(adapter);
        recyclerview.animate();
    }


    private MyListener.MenuListener getMenuListener (){
        return new MyListener.MenuListener() {
            @Override
            public void onMenuListenerCallback(String callback) {
                if (callback.equalsIgnoreCase("Let's Play")){
                    initLetsPlayModule();
                }
                else if (callback.equalsIgnoreCase("High Scores")){
                    initHighScores();
                }
                else if (callback.equalsIgnoreCase("Tutorial")){
                    Intent intent = new Intent(WelcomeScreenActivity.this,TutorialActivity.class);
                    startActivity(intent);
                }
                else if (callback.equalsIgnoreCase("About Us")){
                    Intent intent = new Intent(WelcomeScreenActivity.this,AboutUsActivity.class);
                    startActivity(intent);
                }
            }
        };
    }

    private void initLetsPlayModule(){
        title.setText("Operations");

        SubjectRecyclerViewAdapter adapter = new SubjectRecyclerViewAdapter(getSubjects(),this,getSubjectListener());

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        recyclerview.setLayoutManager(layoutManager);
        recyclerview.setAdapter(adapter);

        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initMainMenu();
            }
        });
    }

    private void initHighScores(){
        title.setText("High Scores");

        ScoresRecyclerViewAdapter adapter = new ScoresRecyclerViewAdapter(DataManager.getInstance().getScores(),this,getSubjectListener());
        recyclerview.setLayoutManager(new GridLayoutManager(this,1));
        recyclerview.setAdapter(adapter);

        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initMainMenu();
            }
        });
    }

    private ArrayList<String> getSubjects(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Addition");
        arrayList.add("Subtraction");
        arrayList.add("Multiplication");
        arrayList.add("Division");
        arrayList.add("Random");

        return arrayList;
    }

    private MyListener.SubjectListener getSubjectListener (){
        return new MyListener.SubjectListener() {
            @Override
            public void onSubjectListener(String Callback) {
                DataManager.getInstance().selectedSubject = Callback;
                initDifficulty();
            }
        };
    }

    private void initDifficulty(){
        title.setText("Difficulty");

        DifficultyRecyclerViewAdapter adapter = new DifficultyRecyclerViewAdapter(getDifficulties(),this,getDifficultyListener());
        recyclerview.setLayoutManager(new GridLayoutManager(this,1));
        recyclerview.setAdapter(adapter);

        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initLetsPlayModule();
            }
        });
    }

    private ArrayList<String> getDifficulties(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Easy");
        arrayList.add("Average");
        arrayList.add("Hard");

        return arrayList;
    }

    private MyListener.DifficultyListener getDifficultyListener (){
        return new MyListener.DifficultyListener() {
            @Override
            public void onDifficultyListener(String Callback) {
                DataManager.getInstance().selectedDifficulty = Callback;
                Intent intent = new Intent(WelcomeScreenActivity.this,QuizActivity.class);
                startActivity(intent);
            }

        };
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (myMediaPlayer!=null){
            myMediaPlayer.pauseMusic();
        }
        System.out.println("WELCOME ON PAUSE");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (myMediaPlayer!=null){
            myMediaPlayer.resumeMusic();
        }
        System.out.println("WELCOME ON RESUME");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myMediaPlayer!=null){
            myMediaPlayer.pauseMusic();
        }
    }
}