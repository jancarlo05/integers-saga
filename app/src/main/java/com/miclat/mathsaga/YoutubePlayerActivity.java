package com.miclat.mathsaga;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miclat.mathsaga.Objects.DataManager;
import com.miclat.mathsaga.Objects.Tools;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.FullscreenListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;

public class YoutubePlayerActivity extends AppCompatActivity {

    YouTubePlayerView youtube_player_view;
    ImageView back;
    LinearLayout container;
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_player);

        container = findViewById(R.id.container);
        back = findViewById(R.id.back);
        youtube_player_view = findViewById(R.id.youtube_player_view);
        textView = findViewById(R.id.textView3);


//        YouTubePlayerView youtube_player_view = new YouTubePlayerView(this);
//        container.addView(youtube_player_view);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getLifecycle().addObserver(youtube_player_view);
        youtube_player_view.addFullscreenListener(new FullscreenListener() {
            @Override
            public void onEnterFullscreen(@NonNull View view, @NonNull Function0<Unit> function0) {
                back.setVisibility(View.GONE);
                textView.setVisibility(View.GONE);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
            @Override
            public void onExitFullscreen() {
                back.setVisibility(View.VISIBLE);
                textView.setVisibility(View.VISIBLE);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        });

        youtube_player_view.setEnableAutomaticInitialization(true);
        youtube_player_view.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                String videoId = DataManager.getInstance().selectedTutorial;
                youTubePlayer.loadVideo(videoId, 0);
            }

            @Override
            public void onError(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerError error) {
                super.onError(youTubePlayer, error);
                youTubePlayer.loadVideo( DataManager.getInstance().selectedTutorial,0);
            }
        });

        String operation = DataManager.getInstance().selectedTutorialOperation;
        View view = null;
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);


        if (operation.equalsIgnoreCase("Addition")){
            view = inflater.inflate(R.layout.addition_tutorial,null,true);
        }
        else if (operation.equalsIgnoreCase("Subtraction")){
            view = inflater.inflate(R.layout.addition_tutorial,null,true);
        }
        else if (operation.equalsIgnoreCase("Multiplication")){
            view = inflater.inflate(R.layout.multiplication_tutorial,null,true);
        }
        else if (operation.equalsIgnoreCase("Division")){
            view = inflater.inflate(R.layout.division_tutorial,null,true);
        }

        if (view!=null){
            container.addView(view);
        }
        if (view!=null){
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
            int pixel = (int) Tools.convertDpToPixel(10,this);
            params.setMargins(pixel,pixel,pixel,pixel);
            view.setLayoutParams(params);
        }
    }

}